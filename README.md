# Docker Multicast Livestream

## Overview
This project is used to be able to livestream to mutliple platforms at once without
having to subscribe to a third party service. Currently is can stream to Facebook and
Youtube only, but can be edited in the `nginx/nginx.conf` file to add in more platforms.
This has only been tested with OBS Studio, but in theory can be run with any software
that will output an RTMP stream.

## Project Setup
1. Download and Install [Docker](https://www.docker.com/get-started).

2. Open terminal window (command prompt on windows) and navigate to project folder. You can
    move the project folder to your Desktop and copy/paste the commands below if you aren't sure
    how to navigate directories from a command line interface.
    
    For Mac/Linux: `cd ~/Desktop/multicast-livestream`
    
    For Windows: `cd Desktop\multicast-livestream`

3. Go to streaming platforms and add streaming link and keys within `nginx/nginx.conf`. For example
    the full link should look something like `rtmp://live-api.link.com/rtmp/streamkey`. 

4. Run `docker-compose up` in command line and you should be set to stream!

## OBS/Livestream Setup
1. Set your streaming software to stream to a custom link.

2. Copy/paste `rtmp://localhost/live` into your streaming software for the streaming link.

3. If your software requires a stream key like OBS does, it doesn't matter what the key is so you can
    type in anything you want.

4. Start Streaming!

